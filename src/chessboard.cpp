/*
	Chess engine in C++
	Copyright (C) 2018 Emanuele Sorce - emanuele.sorce@hotmail.com

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3 or compatibles.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc.
*/

#include "chessboard.hpp"

using namespace std;

#define FORCE_MODE_HACK_MOVE Move(Pos(-8, -8), Pos(-8, -8))
#define FORCE_MODE_HACK_STRING "f"

void Board::printMove(const std::string& s, const Move& m)
{
	stringstream ss;
	ss << "# " << s << ": " << fileToChar(m.piece.file) << m.piece.rank+1 << fileToChar(m.dest.file) << m.dest.rank+1;
	eng->out(ss.str());
}

void Board::quit()
{
	ui_quit = true;
	
	ai.stop();
}

Board::Board(Board& b):
	Position(static_cast<Position>(b)),
	eng(b.eng),
	ai(b.ai),
	move_cache(b.move_cache),
	ui_quit(false),
	force_mode(b.force_mode),
	last_move_capture(b.last_move_capture),
	last_move_check(b.last_move_check)
{}

Board::Board(Engine *const e):
	Position(),
	eng(e),
	ai(eng),
	ui_quit(false),
	force_mode(false),
	last_move_capture(false),
	last_move_check(false)
{
	// set the board starting pieces
	cell[0][0] = Cell(Piece::rock, WHITE);
	cell[0][1] = Cell(Piece::knight, WHITE);
	cell[0][2] = Cell(Piece::bishop, WHITE);
	cell[0][3] = Cell(Piece::queen, WHITE);
	cell[0][4] = Cell(Piece::king, WHITE);
	cell[0][5] = Cell(Piece::bishop, WHITE);
	cell[0][6] = Cell(Piece::knight, WHITE);
	cell[0][7] = Cell(Piece::rock, WHITE);
	for(int i=0; i!=8; i++)
		cell[1][i] = Cell(Piece::pawn, WHITE);
	
	for(int i=0; i!=8; i++)
		cell[6][i] = Cell(Piece::pawn, BLACK);
	cell[7][0] = Cell(Piece::rock, BLACK);
	cell[7][1] = Cell(Piece::knight, BLACK);
	cell[7][2] = Cell(Piece::bishop, BLACK);
	cell[7][3] = Cell(Piece::queen, BLACK);
	cell[7][4] = Cell(Piece::king, BLACK);
	cell[7][5] = Cell(Piece::bishop, BLACK);
	cell[7][6] = Cell(Piece::knight, BLACK);
	cell[7][7] = Cell(Piece::rock, BLACK);
}

void Board::start(const Color& aic)
{
	auto ai_play = [this]()
	{
		Move move = ai.bestMove(*this);
		if(move == MOVE_ABORTED)
			return false;
		doMove(move);
		stringstream ss;
		ss << "move " << fileToChar(move.piece.file) << move.piece.rank+1 << fileToChar(move.dest.file) << move.dest.rank+1;
		eng->out(ss.str());
		return true;
	};
	auto force_play = [this]()
	{
		Move move;
		do
		{
			move = guiPlay();
			if(move == FORCE_MODE_HACK_MOVE)
				return false;
		}
		while (!doMove(move));
		return true;
	};
	auto post_move = [this]()
	{
		vector<Move> moves;
		getAllLegalMoves(&moves, turn);
		bool checked = isInCheck(turn);
		if(moves.empty())
		{
			if(checked)
				eng->out("# checkmate");
			else
				eng->out("# stalemate");
			
			enterForceMode();
			
			return;
		}
		else if(checked)
			eng->out("# check");
	};
	
	ai_color = aic;
	
	while(1)
	{
		if(ai_color == turn && force_mode == false)
		{
			if(ai_play())
				post_move();
		}
		else
			if(force_play())
				post_move();
		
		if(ui_quit)
			return;
	}
}

void Board::forceMove(const Move& move)
{
	// reset cache of pieces in the same rank, file, diagonals and the knight range of both piece and dest
	auto cache_reset = [this](const Pos& pos)
	{
		cache[pos.rank][pos.file].reset();
		
		int i;
		// same file
		for(i=pos.rank+1; i!=8; ++i)
			if(cell[i][pos.file].piece != Piece::empty)
			{
				if
				(
					cell[i][pos.file].piece == Piece::queen ||
					cell[i][pos.file].piece == Piece::rock ||
					cell[i][pos.file].piece == Piece::king ||
					cell[i][pos.file] == Cell(PAWN, BLACK)
				)
					cache[i][pos.file].reset();
				break;
			}
		for(i=pos.rank-1; i!=-1; --i)
			if(cell[i][pos.file].piece != Piece::empty)
			{
				if
				(
					cell[i][pos.file].piece == Piece::queen ||
					cell[i][pos.file].piece == Piece::rock ||
					cell[i][pos.file].piece == Piece::king ||
					cell[i][pos.file] == Cell(PAWN, WHITE)
				)
					cache[i][pos.file].reset();
				break;
			}
		// same rank
		for(i=pos.file+1; i!=8; ++i)
			if(cell[pos.rank][i].piece != Piece::empty)
			{
				if
				(
					cell[pos.rank][i].piece == Piece::queen ||
					cell[pos.rank][i].piece == Piece::rock ||
					cell[pos.rank][i].piece == Piece::pawn ||
					cell[pos.rank][i].piece == Piece::king
				)
					cache[pos.rank][i].reset();
				break;
			}
		for(i=pos.file-1; i!=-1; --i)
			if(cell[pos.rank][i].piece != Piece::empty)
			{
				if
				(
					cell[pos.rank][i].piece == Piece::queen ||
					cell[pos.rank][i].piece == Piece::rock ||
					cell[pos.rank][i].piece == Piece::pawn ||
					cell[pos.rank][i].piece == Piece::king
				)
					cache[pos.rank][i].reset();
				break;
			}
		// diagonals
		for
		(
			Pos p = Pos(pos.rank+1, pos.file+1);
			p.rank != 8 && p.file != 8;
			++p.rank, ++p.file
		)
		{
			if(cell[p.rank][p.file].piece != Piece::empty)
			{
				if
				(
					cell[p.rank][p.file].piece == Piece::queen ||
					cell[p.rank][p.file].piece == Piece::bishop ||
					cell[p.rank][p.file].piece == Piece::king ||
					cell[p.rank][p.file].piece == Piece::pawn
				)
				{
					cache[p.rank][p.file].reset();
				}
				break;
			}
		}
		for
		(
			Pos p = Pos(pos.rank+1, pos.file-1);
			p.rank != 8 && p.file != -1;
			++p.rank, --p.file
		)
		{
			if(cell[p.rank][p.file].piece != Piece::empty)
			{
				if
				(
					cell[p.rank][p.file].piece == Piece::queen ||
					cell[p.rank][p.file].piece == Piece::bishop ||
					cell[p.rank][p.file].piece == Piece::king ||
					cell[p.rank][p.file].piece == Piece::pawn
				)
					cache[p.rank][p.file].reset();
				break;
			}
		}
		for
		(
			Pos p = Pos(pos.rank-1, pos.file-1);
			p.rank != -1 && p.file != -1;
			--p.rank, --p.file
		)
		{
			if(cell[p.rank][p.file].piece != Piece::empty)
			{
				if
				(
					cell[p.rank][p.file].piece == Piece::queen ||
					cell[p.rank][p.file].piece == Piece::bishop ||
					cell[p.rank][p.file].piece == Piece::king ||
					cell[p.rank][p.file].piece == Piece::pawn
				)
					cache[p.rank][p.file].reset();
				break;
			}
		}
		for
		(
			Pos p = Pos(pos.rank-1, pos.file+1);
			p.rank != -1 && p.file != 8;
			--p.rank, ++p.file
		)
		{
			if(cell[p.rank][p.file].piece != Piece::empty)
			{
				if
				(
					cell[p.rank][p.file].piece == Piece::queen ||
					cell[p.rank][p.file].piece == Piece::bishop ||
					cell[p.rank][p.file].piece == Piece::king ||
					cell[p.rank][p.file].piece == Piece::pawn
				)
					cache[p.rank][p.file].reset();
				break;
			}
		}
		// knight moves
		Pos kpos[8];
		kpos[0] = Pos(pos.rank+2, pos.file+1);
		kpos[1] = Pos(pos.rank+2, pos.file-1);
		kpos[2] = Pos(pos.rank-2, pos.file+1);
		kpos[3] = Pos(pos.rank-2, pos.file-1);
		kpos[4] = Pos(pos.rank+1, pos.file+2);
		kpos[5] = Pos(pos.rank+1, pos.file-2);
		kpos[6] = Pos(pos.rank-1, pos.file+2);
		kpos[7] = Pos(pos.rank-1, pos.file-2);
		for(i=0; i!=8; i++)
		{
			if(kpos[i].isValid())
				if(at(kpos[i]).piece == Piece::knight)
					cache[kpos[i].rank][kpos[i].file].reset();
		}
		// So that kings can notice when they can castle
		if(white_king_castle || white_queen_castle)
			cache[0][4].reset();
		if(black_king_castle || black_queen_castle)
			cache[7][4].reset();
	};
	
	//------------------------------------------------------------------
	last_move_capture = false;
	
	// castling king side
	if( move == Move(Pos(0,4),Pos(0,6)) )
	{
		cell[0][4].piece = Piece::empty;
		cell[0][7].piece = Piece::empty;
		cell[0][5].piece = Piece::rock;
		cell[0][5].color = turn;
		cell[0][6].piece = Piece::king;
		cell[0][6].color = turn;
			
		white_king_castle = false;
		white_queen_castle = false;
		
		// extra reset
		cache_reset(Pos(0,5));
		cache_reset(Pos(0,7));
	}
	else if( move == Move(Pos(7,4),Pos(7,6)) )
	{
		cell[7][4].piece = Piece::empty;
		cell[7][7].piece = Piece::empty;
		cell[7][5].piece = Piece::rock;
		cell[7][5].color = turn;
		cell[7][6].piece = Piece::king;
		cell[7][6].color = turn;
			
		black_king_castle = false;
		black_queen_castle = false;
			
		cache_reset(Pos(7,5));
		cache_reset(Pos(7,7));
	}
	// castling queen side
	else if( move == Move(Pos(0,4),Pos(0,2)) )
	{
		cell[0][4].piece = Piece::empty;
		cell[0][0].piece = Piece::empty;
		cell[0][3].piece = Piece::rock;
		cell[0][3].color = turn;
		cell[0][2].piece = Piece::king;
		cell[0][2].color = turn;
			
		white_king_castle = false;
		white_queen_castle = false;
		
		// extra resets
		cache_reset(Pos(0,0));
		cache_reset(Pos(0,1));
		cache_reset(Pos(0,3));
	}
	else if( move == Move(Pos(7,4),Pos(7,2)) )
	{
		cell[7][4].piece = Piece::empty;
		cell[7][0].piece = Piece::empty;
		cell[7][3].piece = Piece::rock;
		cell[7][3].color = turn;
		cell[7][2].piece = Piece::king;
		cell[7][2].color = turn;
			
		black_king_castle = false;
		black_queen_castle = false;
		
		// extra resets
		cache_reset(Pos(7,0));
		cache_reset(Pos(7,1));
		cache_reset(Pos(7,3));
	}
	// do simple move
	else
	{
		// it's a capture? (not consider en passant)
		if(at(move.dest).piece != Piece::empty)
			last_move_capture = true;
		
		// if it's a en passant capture
		if
		(
			at(move.piece).piece == Piece::pawn &&
			at(move.dest).piece == Piece::empty &&
			move.dest.file != move.piece.file
		)
		{
			int direction = at(move.piece).color == WHITE ? 1 : -1;
			
			// make some extra things before standard routines
			last_move_capture = true;
			cell[move.dest.rank-direction][move.dest.file].piece = Piece::empty;
			cache_reset(Pos(move.dest.rank-direction, move.dest.file));
		}
		
		// if it's the first double step pawn move - update en passant right
		if
		(
			at(move.piece).piece == Piece::pawn &&
			move.piece.rank == ( at(move.piece).color == WHITE ? 1 : 6 ) &&
			move.dest.rank == ( at(move.piece).color == WHITE ? 3 : 4 )
		)
		{
			at(move.piece).color == WHITE ?
				white_en_passant = move.piece.file:
				black_en_passant = move.piece.file;
		}
		else
			(at(move.piece).color == WHITE ? white_en_passant : black_en_passant) = -1;
		
		// do it
		at(move.dest).piece = at(move.piece).piece;
		at(move.dest).color = at(move.piece).color;
		at(move.piece).piece = Piece::empty;
		
		// promotion
		if
		(
			at(move.dest).piece == Piece::pawn &&
			(
				(turn == WHITE) ?
					(move.dest.rank == 7) :
					(move.dest.rank == 0)
			)
		)
			at(move.dest).piece = Piece::queen;
		// update castling rights
		if(white_king_castle || white_queen_castle)
		{
			if(cell[0][4].piece != Piece::king)
			{
				white_king_castle = false;
				white_queen_castle = false;
			}
			else if(cell[0][0].piece != Piece::rock || cell[0][0].color != WHITE)
				white_queen_castle = false;
			else if(cell[0][7].piece != Piece::rock || cell[0][7].color != WHITE)
				white_king_castle = false;
		}
		if(black_king_castle || black_queen_castle)
		{
			if(cell[7][4].piece != Piece::king)
			{
				black_king_castle = false;
				black_queen_castle = false;
			}
			else if(cell[7][0].piece != Piece::rock || cell[7][0].color != BLACK)
				black_queen_castle = false;
			else if(cell[7][7].piece != Piece::rock || cell[7][7].color != BLACK)
				black_king_castle = false;
		}
	}
	
	// reset move caches
	cache_reset(Pos(move.piece.rank, move.piece.file));
	cache_reset(Pos(move.dest.rank, move.dest.file));
	move_cache.reset();
	
	// update turn
	turn = !turn;
	isInCheck(turn) ?
		last_move_check = true:
		last_move_check = false;
	++age;
}

bool Board::doMove(const Move& move)
{
	bool legal = false;
	// collect legal moves
	
	// sanitize input
	if
	(	! (
		move.piece.isValid() &&
		move.dest.isValid() )
	)
	{
		std::cout << "# illegal move (not in the board) - raw move data:" << move.piece.file << "|" << move.piece.rank << "|" << move.dest.file << "|" << move.dest.rank << endl;
		return false;
	}
	
	// list of all the legal moves
	vector<Move> legal_moves;
	getPieceMoves(&legal_moves, move.piece);
	filterCheck(&legal_moves, turn);
	
	// check if move is legal
	for(unsigned int i=0; i!=legal_moves.size(); i++)
		if(legal_moves[i] == move)
			legal = true;
	
	// do it
	if(legal)
	{
		forceMove(move);
		return true;
	}
	
	printMove("illegal move", move);
	
	return false;
}

void Board::filterCheck(vector<Move>* moves, const Color& color)
{
	auto it = moves->begin();
	while(it != moves->end())
	{
		Board board(*this);
		board.forceMove(*it);
		if( !board.isInCheck(color) )
			it++;
		else
			it = moves->erase(it);
	}
}

bool Board::isInCheck(const Color& color)
{
	// King position
	Pos king;
	
	// @todo: cache kings position?
	// find king of that color
	for(king.rank=0; king.rank!=8; king.rank++)
	{
		for(king.file=0; king.file!=8; king.file++)
		{
			if( at(king) == Cell(KING, color) )
				goto found;
		}
	}
	found:
	
	// check for all adversary moves that may check the king
	// same rank
	Pos can(king.rank, king.file+1);
	for(;can.file!=8; ++can.file)
	{
		if
		(
			at(can).color != color &&
			(
				at(can).piece == ROCK ||
				at(can).piece == QUEEN ||
				(at(can).piece == KING && can.file-king.file == 1)
			)
		)
			return true;
		else if(at(can).piece != EMPTY)
			break;
	}
	for(can.file=king.file-1; can.file!=-1; --can.file)
	{
		if
		(
			at(can).color != color &&
			(
				at(can).piece == ROCK ||
				at(can).piece == QUEEN ||
				(at(can).piece == KING && can.file-king.file == -1)
			)
		)
			return true;
		else if(at(can).piece != EMPTY)
			break;
	}
	// same file
	can = Pos(king.rank+1, king.file);
	for(;can.rank!=8; ++can.rank)
	{
		if
		(
			at(can).color != color &&
			(
				at(can).piece == ROCK ||
				at(can).piece == QUEEN ||
				(at(can).piece == KING && can.rank-king.rank == 1)
			)
		)
			return true;
		else if(at(can).piece != EMPTY)
			break;
	}
	for(can.rank=king.rank-1; can.rank!=-1; --can.rank)
	{
		if
		(
			at(can).color != color &&
			(
				at(can).piece == ROCK ||
				at(can).piece == QUEEN ||
				(at(can).piece == KING && can.rank-king.rank == -1)
			)
		)
			return true;
		else if(at(can).piece != EMPTY)
			break;
	}
	// diagonals
	for
	(
		can = Pos(king.rank+1, king.file+1);
		can.rank != 8 && can.file != 8;
		++can.rank, ++can.file
	)
	{
		if
		(
			at(can).color != color &&
			(
				at(can).piece == BISHOP ||
				at(can).piece == QUEEN ||
				(at(can).piece == KING && can.rank-king.rank == 1) ||
				(color == WHITE && can.rank-king.rank == 1 && at(can).piece == PAWN)
			)
		)
			return true;
		else if(at(can).piece != EMPTY)
			break;
	}
	for
	(
		can = Pos(king.rank-1, king.file-1);
		can.rank != -1 && can.file != -1;
		--can.rank, --can.file
	)
	{
		if
		(
			at(can).color != color &&
			(
				at(can).piece == BISHOP ||
				at(can).piece == QUEEN ||
				(at(can).piece == KING && can.rank-king.rank == -1) ||
				(color == BLACK && can.rank-king.rank == -1 && at(can).piece == PAWN)
			)
		)
			return true;
		else if(at(can).piece != EMPTY)
			break;
	}
	//
	for
	(
		can = Pos(king.rank+1, king.file-1);
		can.rank != 8 && can.file != -1;
		++can.rank, --can.file
	)
	{
		if
		(
			at(can).color != color &&
			(
				at(can).piece == BISHOP ||
				at(can).piece == QUEEN ||
				(at(can).piece == KING && can.rank-king.rank == 1) ||
				(color == WHITE && can.rank-king.rank == 1 && at(can).piece == PAWN)
			)
		)
			return true;
		else if(at(can).piece != EMPTY)
			break;
	}
	for
	(
		can = Pos(king.rank-1, king.file+1);
		can.rank != -1 && can.file != 8;
		--can.rank, ++can.file
	)
	{
		if
		(
			at(can).color != color &&
			(
				at(can).piece == BISHOP ||
				at(can).piece == QUEEN ||
				(at(can).piece == KING && can.rank-king.rank == -1) ||
				(color == BLACK && can.rank-king.rank == -1 && at(can).piece == PAWN)
			)
		)
			return true;
		else if(at(can).piece != EMPTY)
			break;
	}
	// knights
	if( Pos(king.rank+1, king.file+2).isValid() &&
		at(king.rank+1, king.file+2) == Cell(KNIGHT, !color) )
		return true;
	if( Pos(king.rank+1, king.file-2).isValid() &&
		at(king.rank+1, king.file-2) == Cell(KNIGHT, !color) )
		return true;
	if( Pos(king.rank-1, king.file+2).isValid() &&
		at(king.rank-1, king.file+2) == Cell(KNIGHT, !color) )
		return true;
	if( Pos(king.rank-1, king.file-2).isValid() &&
		at(king.rank-1, king.file-2) == Cell(KNIGHT, !color) )
		return true;
	if( Pos(king.rank+2, king.file+1).isValid() &&
		at(king.rank+2, king.file+1) == Cell(KNIGHT, !color) )
		return true;
	if( Pos(king.rank+2, king.file-1).isValid() &&
		at(king.rank+2, king.file-1) == Cell(KNIGHT, !color) )
		return true;
	if( Pos(king.rank-2, king.file+1).isValid() &&
		at(king.rank-2, king.file+1) == Cell(KNIGHT, !color) )
		return true;
	if( Pos(king.rank-2, king.file-1).isValid() &&
		at(king.rank-2, king.file-1) == Cell(KNIGHT, !color) )
		return true;
	
	// no checks
	return false;
}

bool Board::isMated(const Color& color)
{
	if( isInCheck(color) )
	{
		unsigned int moves = getAllLegalMovesNum(color);
	
		if(moves == 0)
			return true;
	}
	return false;
}

void Board::getPieceMoves(vector<Move>* moves, const Pos& p)
{
	if(!cache[p.rank][p.file])
	{
		cache[p.rank][p.file] = vector<Move>();
		// generate moves, and save them in the cache
		switch(at(p).piece)
		{			
			case Piece::pawn:
				getPawnMoves(&cache[p.rank][p.file].value(), p);
				break;
			case Piece::knight:
				getKnightMoves(&cache[p.rank][p.file].value(), p);
				break;
			case Piece::bishop:
				getBishopMoves(&cache[p.rank][p.file].value(), p);
				break;
			case Piece::rock:
				getRockMoves(&cache[p.rank][p.file].value(), p);
				break;
			case Piece::queen:
				getQueenMoves(&cache[p.rank][p.file].value(), p);
				break;
			case Piece::king:
				getKingMoves(&cache[p.rank][p.file].value(), p);
				break;
			case Piece::empty:
				break;
		}
	}
	
	if(moves != nullptr)
		*moves = cache[p.rank][p.file].value();
}

void Board::getPawnMoves(vector<Move> *moves, const Pos& p)
{
	// color of the piece
	Color color = at(p).color;
	
	// moving direction of pawns
	short dir =
		color == WHITE ? +1 : -1;
	
	// single step if we can
	if(cell[p.rank+dir][p.file].piece == EMPTY)
		moves->push_back(Move(p, Pos(p.rank+dir, p.file)));
	
	// lateral capture
	if( p.file < 7 )
	{
		Pos target(p.rank+dir, p.file+1);
		
		if
		(
			// cell must not be empty
			at(target).piece != Piece::empty &&
			// piece must have opposite color
			at(target).color != color
		)
			moves->push_back(Move(p, target));
	}
	if( p.file > 0 )
	{
		Pos target(p.rank+dir, p.file-1);
		
		if
		(
			// pell must not be empty
			at(target).piece != Piece::empty &&
			// piece must have opposite color
			at(target).color != color
		)
			moves->push_back(Move(p, target));
	}
	// en passant capture
	if
	(
		// right rank
		(color == WHITE) ? (p.rank == 4) : (p.rank == 3)
	)
	{
		// right side capture
		if
		(
			p.file < 7 &&
			(color == WHITE ? black_en_passant : white_en_passant) == p.file + 1
		)
			moves->push_back(Move(p, Pos(p.rank+dir, p.file+1)));
		// left side capture
		if
		(
			p.file > 0 &&
			(color == WHITE ? black_en_passant : white_en_passant) == p.file - 1
		)
			moves->push_back(Move(p, Pos(p.rank+dir, p.file-1)));
	}
	
	// two steps as first move
	if
	(
		dir == +1 && p.rank == 1 &&
		cell[p.rank+dir][p.file].piece == Piece::empty &&
		cell[p.rank+2*dir][p.file].piece == Piece::empty
	)
		moves->push_back(Move(p, Pos(p.rank+2*dir, p.file)));
	else if
	(
		dir == -1 && p.rank == 6 &&
		cell[p.rank+dir][p.file].piece == Piece::empty &&
		cell[p.rank+2*dir][p.file].piece == Piece::empty
	)
		moves->push_back(Move(p, Pos(p.rank+2*dir, p.file)));
}

// helper function 
bool Board::candidateMove(const Move& m, vector<Move>* buf)
{
	if( m.piece.isValid() && m.dest.isValid() )
	{
		// if is an empty cell, or one occupied by an enemy piece it's legal
		if
		(
			at(m.dest).piece == EMPTY ||
			at(m.dest).color != at(m.piece).color
		)
		{
			buf->push_back(m);
			// return true only if empty
			if(at(m.dest).piece == Piece::empty)
				return true;
		}
	}
	return false;
};
void Board::getKnightMoves(vector<Move>* moves, const Pos& p)
{
	// get all the possible 8 positions
	candidateMove(Move(p, Pos(p.rank+2, p.file+1)), moves);
	candidateMove(Move(p, Pos(p.rank+2, p.file-1)), moves);
	candidateMove(Move(p, Pos(p.rank-2, p.file+1)), moves);
	candidateMove(Move(p, Pos(p.rank-2, p.file-1)), moves);
	candidateMove(Move(p, Pos(p.rank+1, p.file+2)), moves);
	candidateMove(Move(p, Pos(p.rank+1, p.file-2)), moves);
	candidateMove(Move(p, Pos(p.rank-1, p.file+2)), moves);
	candidateMove(Move(p, Pos(p.rank-1, p.file-2)), moves);
}

void Board::getBishopMoves(vector<Move>* moves, const Pos& p)
{
	int i = 0;
	
	do
		++i;
	while(candidateMove(Move(p, Pos(p.rank+i, p.file+i)), moves));
	
	i=0;
	do
		++i;
	while(candidateMove(Move(p, Pos(p.rank+i, p.file-i)), moves));
	
	i=0;
	do
		++i;
	while(candidateMove(Move(p, Pos(p.rank-i, p.file-i)), moves));
	
	i=0;
	do
		++i;
	while(candidateMove(Move(p, Pos(p.rank-i, p.file+i)), moves));
}

void Board::getRockMoves(vector<Move>* moves, const Pos& p)
{
	int i = 0;
	
	do
		++i;
	while(candidateMove(Move(p, Pos(p.rank, p.file+i)), moves));
	
	i=0;
	do
		++i;
	while(candidateMove(Move(p, Pos(p.rank, p.file-i)), moves));
	
	i=0;
	do
		++i;
	while(candidateMove(Move(p, Pos(p.rank+i, p.file)), moves));
	
	i=0;
	do
		++i;
	while(candidateMove(Move(p, Pos(p.rank-i, p.file)), moves));
}

void Board::getQueenMoves(vector<Move>* moves, const Pos& p)
{
	getBishopMoves(moves, p);
	getRockMoves(moves, p);
}

void Board::getKingMoves(vector<Move>* moves, const Pos& p)
{
	candidateMove(Move(p, Pos(p.rank+1, p.file-1)), moves);
	candidateMove(Move(p, Pos(p.rank+1, p.file)), moves);
	candidateMove(Move(p, Pos(p.rank+1, p.file+1)), moves);
	candidateMove(Move(p, Pos(p.rank, p.file+1)), moves);
	candidateMove(Move(p, Pos(p.rank-1, p.file+1)), moves);
	candidateMove(Move(p, Pos(p.rank-1, p.file)), moves);
	candidateMove(Move(p, Pos(p.rank-1, p.file-1)), moves);
	candidateMove(Move(p, Pos(p.rank, p.file-1)), moves);
	
	Color color = at(p).color;
	
	// king side castling
	if(color == WHITE ? white_king_castle : black_king_castle)
	{
		if(!isInCheck(color))
		{
			int home = (color == WHITE) ? 0 : 7;
		
			// free space in between
			if( cell[home][5].piece == EMPTY &&
				cell[home][6].piece == EMPTY )
			{
				// no checks
				Board var(*this);
				var.forceMove(Move(Pos(home,4),Pos(home,5)));
		
				if( !var.isInCheck(color) )
					moves->push_back(Move(Pos(home,4),Pos(home,6)));
			}
		}
	}
	// queen side castling
	if(color == WHITE ? white_queen_castle : black_queen_castle)
	{
		if(!isInCheck(color))
		{
			int home = (color == WHITE) ? 0 : 7;
		
			// free space in between
			if( cell[home][1].piece == Piece::empty &&
				cell[home][2].piece == Piece::empty &&
				cell[home][3].piece == Piece::empty )
			{
				// no checks
				Board var(*this);
				var.forceMove(Move(Pos(home,4),Pos(home,3)));
	
				if( !var.isInCheck(color) )
					moves->push_back(Move(Pos(home,4),Pos(home,2)));
			}
		}
	}
}


char Board::fileToChar(const int& r)
{
	if(r == 0)
		return 'a';
	if(r == 1)
		return 'b';
	if(r == 2)
		return 'c';
	if(r == 3)
		return 'd';
	if(r == 4)
		return 'e';
	if(r == 5)
		return 'f';
	if(r == 6)
		return 'g';
	if(r == 7)	
		return 'h';
	else
		return 'z';
}

int Board::toFile(const char& l)
{
	if(l == 'a')
		return 0;
	if(l == 'b')
		return 1;
	if(l == 'c')
		return 2;
	if(l == 'd')
		return 3;
	if(l == 'e')
		return 4;
	if(l == 'f')
		return 5;
	if(l == 'g')
		return 6;
	if(l == 'h')
		return 7;
	else
		return 9;
}

void Board::putMove(const std::string& move)
{
	// wait for the previous move to be get
	std::unique_lock<std::mutex> lck(ui_move_mtx);
	
	ui_move = move;
	ui_move_ready = true;
	
	ui_moved.notify_all();
}

Move Board::guiPlay()
{
	// wait for the move to happen
	
	int piece_rank;
	char piece_file;
	int dest_rank;
	char dest_file;
	
	stringstream ss;
	
	std::unique_lock<std::mutex> lck(ui_move_mtx);
	while (!ui_move_ready)
		ui_moved.wait(lck);
	
	// the move that will be sent
	Move move;
	
	// a normal move
	if(ui_move != FORCE_MODE_HACK_STRING)
	{
		ss << ui_move;
		ui_move_ready = false;
	
		ss >> piece_file >> piece_rank >> dest_file >> dest_rank;
	
		move.piece.rank = piece_rank;
		move.dest.rank = dest_rank;
	
		// index start at 0
		--move.piece.rank;
		--move.dest.rank;
	
		move.piece.file = toFile(piece_file);
		move.dest.file = toFile(dest_file);
	}
	// the force mode hack
	else
	{
		move = FORCE_MODE_HACK_MOVE;
	}
	
	return move;
}

void Board::getAllLegalMoves(vector<Move>* dest, const Color& color)
{
	if(!move_cache)
	{
		move_cache = vector<Move>();
		
		for(int i=0; i!=8; i++)
		{
			for(int j=0; j!=8; j++)
			{
				if( cell[i][j].piece == EMPTY )
					continue;
				
				if( cell[i][j].color == color )
				{
					if(!cache[i][j])
						getPieceMoves(nullptr, Pos(i,j));
				
					VEC_APPEND(move_cache.value(), cache[i][j].value());
				}
			}
		}
	}
	
	if(dest != nullptr)
	{
		*dest = move_cache.value();
		filterCheck(dest, color);
	}
}

unsigned int Board::getAllLegalMovesNum(const Color& color)
{
	if(!move_cache)
		getAllLegalMoves(nullptr, color);
	return move_cache->size();
}

void Board::enterForceMode()
{
	ai.stop();
	force_mode = true;
}

void Board::quitForceMode()
{
	force_mode = false;
	ai_color = turn;
	
	putMove( FORCE_MODE_HACK_STRING );
}

void Board::setFEN(const std::string& fen)
{
	// preliminary things
	// reset caches
	for(int i=0; i!=8; i++)
		for(int j=0; j!=8; j++)
			cache[i][j].reset();
	move_cache.reset();
	
	// char to read in fen string
	unsigned int cursor=0;
	
	// the eight ranks (read in big-endian)
	for(int i=7; i!=-1; --i)
	{
		int file = 0;
		
		do
		{
			switch( fen[cursor] )
			{
				case 'P':
					at(i,file) = Cell(PAWN, WHITE);
					break;
				case 'p':
					at(i,file) = Cell(PAWN, BLACK);
					break;
				case 'N':
					at(i,file) = Cell(KNIGHT, WHITE);
					break;
				case 'n':
					at(i,file) = Cell(KNIGHT, BLACK);
					break;
				case 'B':
					at(i,file) = Cell(BISHOP, WHITE);
					break;
				case 'b':
					at(i,file) = Cell(BISHOP, BLACK);
					break;
				case 'R':
					at(i,file) = Cell(ROCK, WHITE);
					break;
				case 'r':
					at(i,file) = Cell(ROCK, BLACK);
					break;
				case 'Q':
					at(i,file) = Cell(QUEEN, WHITE);
					break;
				case 'q':
					at(i,file) = Cell(QUEEN, BLACK);
					break;
				case 'K':
					at(i,file) = Cell(KING, WHITE);
					break;
				case 'k':
					at(i,file) = Cell(KING, BLACK);
					break;
				case '1':
					at(i,file).piece = Piece::empty;
					break;
				case '2':
					at(i,file).piece = Piece::empty;
					at(i,file+1).piece = Piece::empty;
					file++;
					break;
				case '3':
					at(i,file).piece = Piece::empty;
					at(i,file+1).piece = Piece::empty;
					at(i,file+2).piece = Piece::empty;
					file += 2;
					break;
				case '4':
					at(i,file).piece = Piece::empty;
					at(i,file+1).piece = Piece::empty;
					at(i,file+2).piece = Piece::empty;
					at(i,file+3).piece = Piece::empty;
					file += 3;
					break;
				case '5':
					at(i,file).piece = Piece::empty;
					at(i,file+1).piece = Piece::empty;
					at(i,file+2).piece = Piece::empty;
					at(i,file+3).piece = Piece::empty;
					at(i,file+4).piece = Piece::empty;
					file += 4;
					break;
				case '6':
					at(i,file).piece = Piece::empty;
					at(i,file+1).piece = Piece::empty;
					at(i,file+2).piece = Piece::empty;
					at(i,file+3).piece = Piece::empty;
					at(i,file+4).piece = Piece::empty;
					at(i,file+5).piece = Piece::empty;
					file += 5;
					break;
				case '7':
					at(i,file).piece = Piece::empty;
					at(i,file+1).piece = Piece::empty;
					at(i,file+2).piece = Piece::empty;
					at(i,file+3).piece = Piece::empty;
					at(i,file+4).piece = Piece::empty;
					at(i,file+5).piece = Piece::empty;
					at(i,file+6).piece = Piece::empty;
					file += 6;
					break;
				case '8':
					at(i,file).piece = Piece::empty;
					at(i,file+1).piece = Piece::empty;
					at(i,file+2).piece = Piece::empty;
					at(i,file+3).piece = Piece::empty;
					at(i,file+4).piece = Piece::empty;
					at(i,file+5).piece = Piece::empty;
					at(i,file+6).piece = Piece::empty;
					at(i,file+7).piece = Piece::empty;
					file += 7;
					break;
			}
			file++;
			cursor++;
		}
		while( file < 8 );
		
		cursor++;
	}
	
	// side to move
	if( fen[cursor] == 'w')
		turn = WHITE;
	else
		turn = BLACK;
	cursor += 2;
	
	// castling right
	{
		white_king_castle = false;
		white_queen_castle = false;
		black_king_castle = false;
		black_queen_castle = false;
		
		char cast = fen[cursor];
		cursor++;
		
		if(cast != '-')
		{
			while(1)
			{
				if(cast == 'K')
					white_king_castle = true;
				else if(cast == 'k')
					black_king_castle = true;
				else if(cast == 'Q')
					white_queen_castle = true;
				else if(cast == 'q')
					black_queen_castle = true;
				else
					break;
				
				cast = fen[cursor];
				cursor++;
			}
		}
	}
	
	// en-passant
	white_en_passant = -1;
	black_en_passant = -1;
	{
		char enp = fen[cursor];
		cursor++;
		
		if(enp != '-')
		{
			int file = toFile(enp);
			
			char side = fen[cursor];
			cursor++;
			
			if(side == '3')
				white_en_passant = file;
			else
				black_en_passant = file;
		}
	}
	cursor++;
	
	// half-move clock
	// @todo
	while(fen[cursor] != ' ')
		cursor++;
	
	// fullmove counter
	{
		string num;
		
		while( cursor < fen.length() )
		{
			num += fen[cursor];
			cursor++;
		}
			
		age = atoi(num.c_str());
	}
	
	eng->out("# FEN loaded");
}
