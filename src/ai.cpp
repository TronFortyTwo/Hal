/*
	Chess engine in C++
	Copyright (C) 2018 Emanuele Sorce - emanuele.sorce@hotmail.com

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3 or compatibles.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc.
*/

#define AI_DEEP 2

#include "ai.hpp"

#define NO_MOVES_MATE	Move(Pos(-2, -2), Pos(-2, -2))
#define NO_MOVES_DRAW	Move(Pos(-3, -3), Pos(-3, -3))
#define NOT_A_MOVE		Move(Pos(-4, -4), Pos(-4, -4))

#define CHECKMATED_VALUE std::numeric_limits<Score>::min()

using namespace std;

AI::AI(Engine *const e):
	eng(e),
	king_value(0.001),
	queen_value(9),
	rock_value(5),
	bishop_value(3.15),
	knight_value(3),
	pawn_value(1),
	pawn_steps_value_lin(0.1),
	turn_advantage(1.01),
	move_num_value(0.008),
	to_stop(false)
{}

Move AI::bestMove(Board& board)
{
	to_stop = false;
	
	begin_time = clock();
	nodes = 0;
	
	// how long should to think?
	if(eng->time_single_move != -1)
	{
		sec_budget = eng->time_single_move;
	}
	
	Move move = play(board, AI_DEEP);
	
	eng->out(string("# nodes/sec: ") + to_string((nodes * CLOCKS_PER_SEC)/ static_cast<double>(clock()-begin_time)));
	eng->out(string("# total nodes: ") + to_string(nodes));
	
	return move;
}

Move AI::play(Board& board, int deep)
{
	if(to_stop)
	{
		eng->out("# aborted");
		return MOVE_ABORTED;
	}

	Color my_color = board.turn;
	
	// ALL the possible moves
	vector<Move> moves;
	board.getAllLegalMoves(&moves, my_color);
	
	// if mate or stale
	if(moves.empty())
	{
		if(board.isInCheck(my_color))
			return NO_MOVES_MATE;
		else
			return NO_MOVES_DRAW;
	}
	else if(moves.size() == 1)
	{
		if(deep == AI_DEEP)
			eng->out("# forced move - short circuit instead of analysys");
		return moves[0];
	}
	
	Score points;
	
	// the move we are going to do and its evalutation
	Move move = moves[0];
	Score move_points = CHECKMATED_VALUE;
	
	// evalutate all the possible moves
	for(unsigned int i=0; i!=moves.size(); i++)
	{
		valueMove(board, moves[i], my_color, deep, points);
	
		if(points > move_points)
		{
			move_points = points;
			move = moves[i];
		}
		if(deep == AI_DEEP)
		{
			eng->board->printMove("points " + to_string(points), moves[i]);
		}
	}
	
	return move;
}

void AI::valueMove(Board& board, const Move& move, Color color, int deep, Score& value)
{
	Board variation (board);
	variation.forceMove(move);
		
	valueBoard(variation, color, deep, value);
}

void AI::valueBoard
(
	Board& board,
	Color color,	// Which color to evaluate
	int deep,	// How much deeply examine
	Score& value		// the value will be put here
)
{
	++nodes;
	
	// use hash table
	PositionTagData data = eng->hash_table.get(static_cast<Position>(board));
	// hit!
	if( data.deep >= deep )
	{
		(color == WHITE) ?
			value = data.score:
			value = 1/data.score;
		return;
	}
	
	// dynamic evalutation
	if(deep > 0 || ((board.last_move_capture || board.last_move_check) && deep > -AI_DEEP))
	{
		// ------------------------------------
		// who is in turn choose the best move,
		// then value that
		Move best_move = play(board, deep-1);
		
		if(best_move == MOVE_ABORTED)
			return;
		
		if(best_move == NO_MOVES_DRAW)
			value = 1;
			
		else if(best_move == NO_MOVES_MATE)
		{
			if(color != board.turn)
				// infinite value, but do it the shortest way possible
				value = std::numeric_limits<Score>::max() / board.age;
			else
				value = CHECKMATED_VALUE;
		}
		else
			valueMove(board, best_move, color, deep-1, value);
	}
	// static evalutation
	else
	{
		value = staticEval(board, color);
	}
	// add position to table
	data.score = (color == WHITE) ?
		value:
		1/value;
	data.deep = deep;
	
	eng->hash_table.add(static_cast<Position>(board), data);
}

Score AI::staticEval(Board& board, const Color& color)
{
	// if mates
	if(board.turn == color)
	{
		if(board.isMated(!color))
			return std::numeric_limits<Score>::max() / board.age;
	}
	else
	{
		if(board.isMated(color))
			return CHECKMATED_VALUE;
	}
	
	double advantage = 0;
	double disadvantage = 0;
		
	// count each piece
	for(int i=0; i!=8; i++)
	{
		for(int j=0; j!=8; j++)
		{
			if(board.cell[i][j].piece == Piece::empty)
				continue;
			
			if( board.cell[i][j].color == color )
				advantage += getPieceValue(board, Pos(i,j));
			else
				disadvantage += getPieceValue(board, Pos(i,j));
		}
	}
	
	if(board.turn == color)
		advantage *= turn_advantage;
	else
		disadvantage *= turn_advantage;
	
	return advantage/disadvantage;
}

double AI::getPieceValue(Board& board, const Pos& p)
{
	double value;
	Color color = board.at(p).color;
	vector<Move> legal_moves;
	board.getPieceMoves(&legal_moves, p);
	board.filterCheck(&legal_moves, color);
	
	switch( board.at(p).piece )
	{
		case Piece::king:
			value = king_value;
			break;
		case Piece::empty:
			value = 0;
			break;
		case Piece::pawn:
			value = pawn_value;
			// value of the advancing
			color == WHITE ?
				value += pawn_steps_value_lin * MAX(p.rank-4, 0):
				value += pawn_steps_value_lin * MAX(5-p.rank, 0);
			break;
		case Piece::knight:
			value = knight_value;
			break;
		case Piece::bishop:
			value = bishop_value;
			break;
		case Piece::rock:
			value = rock_value;
			break;
		case Piece::queen:
			value = queen_value;
			break;
	}
	
	value += move_num_value * legal_moves.size() * value;
	
	return value;
}

void AI::stop()
{
	to_stop = true;
}
