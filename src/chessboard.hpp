/*
	Chess engine in C++
	Copyright (C) 2018 Emanuele Sorce - emanuele.sorce@hotmail.com

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3 or compatibles.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc.
*/

#ifndef KEYBOARDH
#define KEYBOARDH

#include <iostream>
#include <sstream>
#include <vector>
#include <tuple>
#include <cmath>
#include <optional>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <fstream>

#include "utils.hpp"
#include "interface.hpp"

class Engine;

#include "piece.hpp"
#include "ai.hpp"
#include "table.hpp"

class Board : public Position
{
	// 
	Engine *eng;
	
	// get possible moves for all the pieces
	void getPawnMoves(std::vector<Move>*, const Pos& p);
	void getKnightMoves(std::vector<Move>*, const Pos& p);
	void getBishopMoves(std::vector<Move>*, const Pos& p);
	void getRockMoves(std::vector<Move>*, const Pos& p);
	void getQueenMoves(std::vector<Move>*, const Pos& p);
	void getKingMoves(std::vector<Move>*, const Pos& p);

	// helper function
	bool candidateMove(const Move& m, std::vector<Move>* buf);

	// transform letter to rank
	static int toFile(const char& l);
	static char fileToChar(const int& r);
	
	Move guiPlay();

	AI ai;
	
	// store all the possible moves
	std::optional<std::vector<Move>> move_cache;

	// syncronize threads - the move
	std::string ui_move;
	std::condition_variable ui_moved;
	std::mutex ui_move_mtx;
	bool ui_move_ready = false;

	// if we have to quit
	std::atomic<bool> ui_quit;

	// if force mode is on or off
	bool force_mode;
	
	// what color is AI playing
	Color ai_color;

public:

	inline Cell& at(const Pos& pos)
	{
		return cell[pos.rank][pos.file];
	}
	inline Cell& at(const int& f, const int& r)
	{
		return cell[f][r];
	}

	bool last_move_capture;
	bool last_move_check;
	
	// send a signal to quit asap
	void quit();
	
	// set board to a FEN position
	void setFEN(const std::string&);
	
	// manage force mode
	void enterForceMode();
	void quitForceMode();
	
	// the external interface did a move
	void putMove(const std::string&);
	
	Board(Engine *const);
	Board(Board&);
	
	// start the game
	void start(const Color& ai);
	
	// possible moves cache
	std::optional<std::vector<Move>> cache[8][8];
	
	// make a move -- check that is legal
	bool doMove(const Move& move);
	// just make a move
	void forceMove(const Move& move);

	void filterCheck(std::vector<Move>*, const Color&);

	bool isInCheck(const Color&);
	bool isMated(const Color&);

	void getAllLegalMoves(std::vector<Move>* moves, const Color& color);
	unsigned int getAllLegalMovesNum(const Color& color);
	
	// print a message then a move
	void printMove(const std::string&, const Move&);
		
	// generic - it uses the right function above based on the type of the piece
	void getPieceMoves(std::vector<Move>*, const Pos&);
};


#include "engine.hpp"

#endif
