/*
	Chess engine in C++
	Copyright (C) 2018 Emanuele Sorce - emanuele.sorce@hotmail.com

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3 or compatibles.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc.
*/

#ifndef TABLEH
#define TABLEH

#include <map>
#include <unordered_map>
#include <iostream>
#include <tuple>

#include "piece.hpp"

// a single position kept in the has table
struct Position
{
	// cells
	Cell cell[8][8];
	// who the turn is for
	Color turn;
	// castling rights
	bool white_king_castle;
	bool white_queen_castle;
	bool black_king_castle;
	bool black_queen_castle;
	
	// en-passant rights
	// -1 for no right
	// the file number for the pawn that may be captured with en passant
	int8_t white_en_passant;
	int8_t black_en_passant;
	
	// the age of this position
	int16_t age;
	
	bool operator == (const Position& p) const;
	
	bool operator < (const Position& p) const;
	
	Position();
};

namespace std {

	template <>
	struct hash<Position>
	{
		std::size_t operator()(const Position& p) const
		{
			using std::size_t;
			using std::hash;
			using std::string;

			std::string key;
			key.resize(74);
			
			for(int i=0; i!=8; i++)
				for(int j=0; j!=8; j++)
					key[i*8+j] += p.cell[i][j].getSimbol();
			
			key += p.turn ? 'w' : 'b';
			key += p.white_king_castle ? 't' : 'f';
			key += p.black_king_castle ? 't' : 'f';
			key += p.white_queen_castle ? 't' : 'f';
			key += p.black_queen_castle ? 't' : 'f';
			key += to_string(p.white_en_passant);
			key += to_string(p.black_en_passant);

			return hash<string>()(key);
		}
	};
}

struct PositionTagData
{
	// the score of that position (for white)
	float score;
	// at what deep the position had been analized
	int8_t deep;
	
	PositionTagData();
};

class HashTable
{
	unsigned int max_pos_num;
	
	// our table
	std::unordered_map<Position, PositionTagData> table;
	
public:

	HashTable();

	// set how much memory this table is allowed to use ( in bytes );
	void setMem( const float& b );

	// get position from table
	PositionTagData get(const Position&);
	
	// put new position in the table
	void add(const Position&, const PositionTagData&);

	// reset table
	void clear();
};


#endif
