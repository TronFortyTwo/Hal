/*
	Chess engine in C++
	Copyright (C) 2018 Emanuele Sorce - emanuele.sorce@hotmail.com

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3 or compatibles.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc.
*/

#include "piece.hpp"


bool Pos::operator== (const Pos& m) const
{
	return
		rank == m.rank &&
		file == m.file;
}
bool Pos::operator!= (const Pos& m) const
{
	return
		rank != m.rank ||
		file != m.file;
}

bool Pos::isValid() const
{
	return
		rank <= 7 && rank >= 0 &&
		file <= 7 && file >= 0;
}

bool Move::operator== (const Move& m) const
{
	return
		piece == m.piece &&
		dest == m.dest;
}
bool Move::operator!= (const Move& m) const
{
	return
		piece != m.piece ||
		dest != m.dest;
}

char Cell::getSimbol() const
{
	switch(piece)
	{
		case Piece::empty:
			return ' ';
		case Piece::pawn:
			return 'p';
		case Piece::knight:
			return 'n';
		case Piece::bishop:
			return 'b';
		case Piece::rock:
			return 'r';
		case Piece::queen:
			return 'q';
		case Piece::king:
			return 'k';
	}
}
