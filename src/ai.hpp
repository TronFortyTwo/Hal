/*
	Chess engine in C++
	Copyright (C) 2018 Emanuele Sorce - emanuele.sorce@hotmail.com

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3 or compatibles.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc.
*/

#ifndef AIH
#define AIH

#define MOVE_ABORTED	Move(Pos(-6, -6), Pos(-6, -6))

#include <iostream>
#include <vector>
#include <tuple>
#include <cmath>
#include <thread>
#include <ctime>
#include <cmath>
#include <atomic>
#include <limits>

struct Pos;
struct Move;
class Board;
class Engine;

#include "piece.hpp"

class AI
{
	// seconds we can spend thinking about this move
	double sec_budget;
	
	Engine *eng;
	
	unsigned long int nodes;
	clock_t begin_time;
	
	double king_value;
	double queen_value;
	double rock_value;
	double bishop_value;
	double knight_value;
	double pawn_value;
	
	// more value a pawn gets the more is near the opposite side
	double pawn_steps_value_lin;
	
	// advantage coefficent for who's in turn
	double turn_advantage;
	
	double move_num_value;
	
	void valueBoard(Board&, Color, int, Score& value);
	Score staticEval(Board& board, const Color&);
	double getPieceValue(Board&, const Pos&);
	
	// that is, the value of the board with that move done
	void valueMove(Board&, const Move& move, Color, int, Score&);
	
	// 
	bool to_stop;
	
	Move play(Board&, int deep);
	
public:

	Move bestMove(Board&);
	
	AI(Engine *const);
	
	// signal launched from another thread, it stops as soon as possible any activity
	void stop();
};

#include "engine.hpp"
#include "chessboard.hpp"
#include "table.hpp"

#endif
