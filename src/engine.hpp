/*
	Chess engine in C++
	Copyright (C) 2018 Emanuele Sorce - emanuele.sorce@hotmail.com

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3 or compatibles.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc.
*/

#ifndef ENGINEH
#define ENGINEH

#include <thread>
#include <cmath>
#include <string>
#include <sstream>

#include "chessboard.hpp"
#include "interface.hpp"
#include "table.hpp"

class Engine
{
	bool running;
	
	bool board_thread_running;
	std::thread board_thread;
	
	Interface inter;
	
	// subroutine to stop the board
	void stopBoard();
	
public:

	// time control
	float time_moves;
	float time_base;
	float time_inc;
	float time_single_move;

	Board *board;

	HashTable hash_table;

	Engine();
	
	void parse();
	
	void out(const std::string&);
};

#endif
