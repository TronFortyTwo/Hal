/*
	Chess engine in C++
	Copyright (C) 2018 Emanuele Sorce - emanuele.sorce@hotmail.com

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3 or compatibles.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc.
*/

#include "table.hpp"

using namespace std;

#define DEFAULT_TABLE_SIZE_MB 64.0f

bool Position::operator < (const Position& p) const
{
	return age < p.age;
}
	
Position::Position():
	turn(WHITE),
	white_king_castle(true),
	white_queen_castle(true),
	black_king_castle(true),
	black_queen_castle(true),
	white_en_passant(-1),
	black_en_passant(-1),
	age(0)
{}

bool Position::operator == (const Position& p) const
{
	for(int i=0; i!=8; i++)
		for(int j=0; j!=8; j++)
			if(cell[i][j] != p.cell[i][j])
				return false;
	if
	(
		turn != p.turn ||
		white_en_passant != p.white_en_passant ||
		black_en_passant != p.black_en_passant ||
		white_king_castle != p.white_king_castle ||
		white_queen_castle != p.white_queen_castle ||
		black_king_castle != p.black_king_castle ||
		black_queen_castle != p.black_queen_castle
	)
		return false;

	return true;
}

PositionTagData::PositionTagData():
	score(1),
	deep(-127)
{
}

HashTable::HashTable():
	max_pos_num(DEFAULT_TABLE_SIZE_MB * 1024 * 1024 / sizeof(std::pair<const Position, PositionTagData>) )
{
	cout << "# Hash table: max " << max_pos_num << " nodes" << endl;
}

PositionTagData HashTable::get(const Position& pos)
{
	auto ipos = table.find(pos);
	
	if(ipos == table.end())
		return PositionTagData();
	
	return ipos->second;
}

void HashTable::add(const Position& p, const PositionTagData& ptd)
{
	if(table.size() < max_pos_num)
	{
		table.emplace(p, ptd);
	}
	else
	{
		auto it = table.begin();
		while (it != table.end())
		{
			if ( it->first.age < p.age-1 )
				it = table.erase(it);
			else
				it++;
		}
	}
}

void HashTable::setMem(const float& b)
{
	max_pos_num = b / sizeof(std::pair<const Position, PositionTagData>);
	cout << "# Hash table size update: max " << max_pos_num << " nodes" << endl;
}

void HashTable::clear()
{
	table.clear();
}
