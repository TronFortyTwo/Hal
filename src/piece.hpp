/*
	Chess engine in C++
	Copyright (C) 2018 Emanuele Sorce - emanuele.sorce@hotmail.com

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3 or compatibles.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc.
*/

#ifndef PIECEH
#define PIECEH

#include <string>
#include <vector>
#include <optional>

struct Move;

typedef bool Color;
typedef float Score;

#define WHITE true
#define BLACK false

#define EMPTY	Piece::empty
#define PAWN	Piece::pawn
#define KNIGHT	Piece::knight
#define BISHOP	Piece::bishop
#define ROCK	Piece::rock
#define QUEEN	Piece::queen
#define KING	Piece::king

enum class Piece
{
	empty,
	pawn,
	knight,
	bishop,
	rock,
	queen,
	king
};

struct Pos
{
	int rank;
	int file;
	
	Pos(int r, int f): rank(r), file(f) {}
	Pos(){}
	
	bool operator== (const Pos&) const;
	bool operator!= (const Pos&) const;
	
	bool isValid() const;
};

struct Move
{
	Pos piece;
	Pos dest;
	
	Move(const Pos& p, const Pos& d): piece(p), dest(d) {};
	Move(){}
	
	bool operator== (const Move&) const;
	bool operator!= (const Move&) const;
};

struct Cell
{
	// type of piece
	Piece piece;
	// color of the piece
	Color color;
	
	Cell(const Piece& p, const Color& c):
		piece(p),
		color(c)
	{}
	
	Cell():
		piece(Piece::empty)
	{}
	
	bool operator == (const Cell& c) const
	{
		return (piece == c.piece) && (color == c.color);
	}
	
	bool operator != (const Cell& c) const
	{
		return (piece != c.piece) || (color != c.color);
	}
	
	char getSimbol() const;
};


#endif
