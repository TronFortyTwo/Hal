/*
	Chess engine in C++
	Copyright (C) 2018 Emanuele Sorce - emanuele.sorce@hotmail.com

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3 or compatibles.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc.
*/

#include "engine.hpp"

using namespace std;

Engine::Engine():
	running(true),
	board_thread_running(false),
	time_moves(-1),
	time_base(-1),
	time_inc(-1),
	time_single_move(-1),
	board(new Board(this))
{
	while( running )
	{
		parse();
	}
	
	stopBoard();
	
	delete board;
}

void Engine::stopBoard()
{
	if(board_thread_running)
	{
		board->quit();
		board_thread.join();
		board_thread_running = false;
	}
}

void Engine::out(const std::string& str)
{
	inter.push(str);
}

void Engine::parse()
{
	string in = inter.pull();
	
	if (in == "?" )
	{
		out( string("Error (not implemented): ") + in );
	}
	else if( in == "go")
	{
		board->quitForceMode();
	}
	else if( in == "new" )
	{
		stopBoard();
		
		delete board;
		board = new Board(this);
		
		hash_table.clear();
		
		board_thread_running = true;
		board_thread = std::thread(&Board::start, board, BLACK);
	}
	else if( in == "force" )
	{
		board->enterForceMode();
	}
	else if( in == "random" )
	{
		// random
		out( string("Error (not implemented): ") + in );
	}
	else if( in.substr(0,2) == "st")
	{
		time_single_move = atoi( &in.c_str()[3] );
	}
	else if( in.substr(0,2) == "sd")
	{
		out( string("Error (not implemented): ") + in );
	}
	else if( in.substr(0,4) == "quit")
	{
		running = false;
	}
	else if( in.substr(0,4) == "ping" )
	{
		out( string("pong ") + to_string(atoi(&in.c_str()[5])) );
	}
	else if( in.substr(0,4) == "time" )
	{
		out( string("Error (not implemented): ") + in );
	}
	else if( in.substr(0,4) == "otim" )
	{
		out( string("Error (not implemented): ") + in );
	}
	else if( in.substr(0,4) == "easy")
	{
	}
	else if( in.substr(0,4) == "hard")
	{
		out( string("Error (not implemented): ") + in );
	}
	else if( in.substr(0,4) == "draw")
	{
		out( string("Error (not implemented): ") + in );
	}
	else if( in.substr(0,4) == "post")
	{
		out( string("Error (not implemented): ") + in );
	}
	else if( in.substr(0,5) == "level" )
	{
		// time control
		stringstream ss(&in[6]);
		
		ss >> time_moves;
		ss >> time_base;
		ss >> time_inc;
		
		time_single_move = -1;
	}
	else if( in.substr(0,6) == "memory" )
	{
		stringstream ss(&in[7]);
		
		float mem;
		ss >> mem;
		
		hash_table.setMem(mem * 1024 * 1024);
	}
	else if( in.substr(0,6) == "result" )
	{
		stopBoard();
	}
	else if( in.substr(0,6) == "nopost" )
	{
		out( string("Error (not implemented): ") + in );
	}
	else if( in.substr(0,8) == "setboard")
	{
		board->setFEN( &in[9] );
	}
	else if( in.substr(0,8) == "protover") {}
	else if( in.substr(0,8) == "accepted") {}
	else if( in.substr(0,8) == "rejected") {}
	else if( in.substr(0,8) == "computer") {}
	else if( in.substr(0,8) == "usermove")
	{
		// send move to the engine
		board->putMove(in.substr(8, string::npos));
	}
	else
	{
		out( string("Error (not recognized): ") + in );
	}
}
