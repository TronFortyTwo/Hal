/*
	Chess engine in C++
	Copyright (C) 2018 Emanuele Sorce - emanuele.sorce@hotmail.com

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3 or compatibles.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc.
*/

#include "interface.hpp"

using namespace std;

Interface::Interface()
{
	// unbuffered output
	cout.setf(ios::unitbuf);

	push("# Hal - chess engine in C++");
	push("# Copyright 2018 Emanuele Sorce - emanuele.sorce@hotmail.com");

	// we are in XBoard mode?
	if( pull() != "xboard" )
	{
		cout << "ERR: not in xboard mode!" << endl;
		return;
	}
	// start
	push("feature done=0");
	
	// @todo: this should not be required - but witout it would get stuck in tournaments
	push("feature reuse=0");
	// ping
	push("feature ping=1");
	// setboard instead of edit
	push("feature setboard=1");
	// disable not portable features
	push("feature sigint=0");
	push("feature sigterm=0");
	// name
	push("feature myname=Hal");
	// debug
	push("feature debug=1");
	// old commands
	push("feature colors=0");
	// easyer spotting of xboard moves
	push("feature usermove=1");
	// memory
	push("feature memory=1");
	// analyze mode not yet supported
	push("feature analyze=0");
	
	// done
	push("feature done=1");
}

string Interface::pull()
{
	string temp;
		
	getline(cin, temp);
		
	return temp;
}

void Interface::push(const string& str)
{
	cout << str << '\n';
	cout.flush();
}
