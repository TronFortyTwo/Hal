# Hal - chess engine

A xboard compatible chess engine written in C++.
It's goal is help me research in the chess-engine field.

This engine is licensed under the GNU GPL v3 license.